# MiWiFi R2D remote russifier

## Experimental release warning!

## Usage
Put somewhere on router's hd, preferable to /userdisk/data/russify folder  
```chmod +x russify_net```  
```./russify_net```  

## Credits
I'm not the author of translation, all the fame and glory to galickijj 

Special thanks to 4pda R2D community:
http://4pda.ru/forum/index.php?showtopic=694115

# TODO
* silent mode to use by cron
* detect whether russification was already installed
* may be english version (is anyone want to contribute?)